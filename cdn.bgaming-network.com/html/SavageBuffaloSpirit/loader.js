(() => {
    var e = {};
    e.g = function() {
        if ("object" == typeof globalThis) return globalThis;
        try {
            return this || new Function("return this")()
        } catch (e) {
            if ("object" == typeof window) return window
        }
    }();
    var i;
    (i = "undefined" != typeof window ? window : void 0 !== e.g ? e.g : "undefined" != typeof self ? self : {}).SENTRY_RELEASE = {
        id: "SavageBuffaloSpirit (game: v6.0.58_v14.4.3, utils: v14.4.3)"
    }, i.SENTRY_RELEASES = i.SENTRY_RELEASES || {}, i.SENTRY_RELEASES["games-front-end@sentry"] = {
        id: "SavageBuffaloSpirit (game: v6.0.58_v14.4.3, utils: v14.4.3)"
    }, (() => {
        var e;
        try {
            e = window.localStorage
        } catch (t) {
            console.log("LocalStorage is unavailable!")
        }
        var i = window.SKIN_DIRS || {};

        function o(i) {
            var o = (i.ui.skin || "basic").toLocaleLowerCase(),
                t = window.location.search.match(new RegExp("[?&]skin=([^&]*)(&?)", "i")),
                n = t ? t[1] : null;
            if (!e) return n || o;
            var a = `lastApiSkin_${i.cache_id}`,
                r = `userSkin_${i.cache_id}`;
            return n ? e.setItem(r, n) : n = e.getItem(r), e.getItem(a) === o && n ? n : (e.removeItem(r), e.setItem(a, o), o)
        }
        window.initializeCasinoOptions = e => {
            var t = o(e),
                {
                    root: n,
                    res: a = "v6.0.58_v14.4.3"
                } = i[t] || i.basic || {};
            e.ui.applied_skin = n, e.resources_root_path = e.resources_path + (n ? `/${n}` : ""), e.resources_path += `/${a}`, e.game_bundle_source = e.resources_path + "/bundle.js", window.__OPTIONS__ = e
        }, window.initializeCasinoOptions(window.__OPTIONS__)
    })()
})();
//# sourceMappingURL=./loader.js.map